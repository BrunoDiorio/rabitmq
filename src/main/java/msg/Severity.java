package msg;

public enum Severity {

	ERRO_PARCIAL,
	ERRO_TOTAL,
	ALERTA_N1,
	ALERTA_N2;
}
