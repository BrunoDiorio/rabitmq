package tutorial03;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

public class Recv {

	private static final String EXCHANGE_NAME = "logs";
	
	public static void main(String[] args) {
		
		try{
			
			ConnectionFactory factory = new ConnectionFactory();
			factory.setHost("localhost");
			Connection connection = factory.newConnection();
			Channel channel = connection.createChannel();
			
			channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.FANOUT);

			String queueName = channel.queueDeclare().getQueue();
			
			channel.queueBind(queueName, EXCHANGE_NAME, "");
			
			System.out.println("[*] Waiting for messages. To exit press CTRL+C");
			
			final Consumer consumer = new DefaultConsumer(channel) {
				
				@Override
				public void handleDelivery(String consumerTag, Envelope envelope, BasicProperties properties,byte[] body) throws IOException {
					String message = new String(body, "UTF-8");
					System.out.println("[x] Recived '" + message + "'");
					
					try {
						doWork(message);
					} catch (Exception e) {
						e.printStackTrace();
					}
					System.out.println("[x] done");
					getChannel().basicAck(envelope.getDeliveryTag(), false);
				}
			};
			
			channel.basicConsume(queueName, true, consumer);
			
			
			
			
		}catch(Exception e ){
			e.printStackTrace();
		}

	}
	
	
	private static void doWork( String task )throws InterruptedException{
		for (Character c : task.toCharArray()) {
			if( c.equals('.'))
				Thread.sleep(1000);
		}
	}

}
