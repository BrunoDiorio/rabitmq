package tutorial03;

import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class Send {

	private static final String EXCHANGE_NAME = "logs";
	
	private static final Scanner sc;
	
	static {
		sc = new Scanner(System.in);
	}
	
	public static void main(String[] args) {
		while(true) {
			try {
				
				ConnectionFactory factory = new ConnectionFactory();
				
				factory.setHost("localhost");
				
				Connection connection = factory.newConnection();
				
				Channel channel = connection.createChannel();
				
				channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.FANOUT);
				
				String message = sc.nextLine();	
				
				channel.basicPublish(EXCHANGE_NAME, "", null, message.getBytes());
				
				System.out.println( "[x] Sent '"  + message + "'");
				
				channel.close();
				connection.close();
				
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
